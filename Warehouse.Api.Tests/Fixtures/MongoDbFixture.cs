﻿using Forex.Infrastructure.MongoDb;
using Forex.Infrastructure.MongoDb.Extensions;
using Forex.Infrastructure.MongoDb.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Linq.Expressions;
using Xunit;

namespace Warehouse.Api.IntegrationTests.Fixtures;

public class MongoDbFixture : IAsyncLifetime
{
    private IServiceProvider ServiceProvider { get; }
    public IMongoDbContext MongoDbContext => ServiceProvider.GetRequiredService<IMongoDbContext>();

    private readonly string prefix;

    public MongoDbFixture()
    {
        prefix = Path.GetRandomFileName()
            .Replace(".", "")
            .Substring(0, 8);

        var config = new ConfigurationBuilder()
            .AddEnvironmentVariables("DOTNET_")
            .AddJsonFile("appsettings.json")
            .Build();

        var services = new ServiceCollection()
            .AddOptions()
            .AddLogging()
            .AddSingleton<IConfiguration>(config)
            .AddMongoDbClient()
            .PostConfigure<MongoDbOptions>(p => { p.CollectionNameFormat = $"{prefix}-{{name}}"; })
            .AddTransient<IMongoDatabase>(c =>
            {
                var client = c.GetRequiredService<IMongoClient>();
                var opt = c.GetRequiredService<IOptions<MongoDbOptions>>();
                return client.GetDatabase(opt.Value.DatabaseName);
            });

        ServiceProvider = services.BuildServiceProvider();
    }

    public IMongoCollection<T> GetCollection<T>()
    {
        return MongoDbContext.GetCollection<T>();
    }

    public async Task InsertAsync<T>(params T[] items)
    {
        var collection = GetCollection<T>();
        if (items.Length > 1)
            await collection.InsertManyAsync(items, new InsertManyOptions { IsOrdered = true });
        else
            await collection.InsertOneAsync(items[0]);
    }

    public async Task<List<T>> FindAsync<T>(Expression<Func<T, bool>> filter, CancellationToken cancellationToken = default(CancellationToken))
    {
        var collection = GetCollection<T>();
        return await collection.Find(filter).ToListAsync(cancellationToken);
    }

    public async Task<T> FindFirstOrDefaultAsync<T>(Expression<Func<T, bool>> filter, CancellationToken cancellationToken = default(CancellationToken))
    {
        var collection = GetCollection<T>();
        return await collection.Find(filter).FirstOrDefaultAsync(cancellationToken);
    }

    public async Task CleanAll()
    {
        var client = ServiceProvider.GetRequiredService<IMongoDatabase>();

        var collections = await (await client.ListCollectionNamesAsync()).ToListAsync();
        var deleteTasks = collections
            .Where(c => c.StartsWith(prefix))
            .Select(c => client.DropCollectionAsync(c, new DropCollectionOptions()))
            .ToArray();
        await Task.WhenAll(deleteTasks);
    }

    public async Task PurgeAsync<T>()
    {
        await MongoDbContext.GetCollection<T>()
            .DeleteManyAsync(FilterDefinition<T>.Empty);
    }

    public async Task DisposeAsync()
    {
        await CleanAll();
    }

    public Task InitializeAsync()
    {
        return Task.CompletedTask;
    }
}

