﻿using MassTransit;
using MassTransit.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.IntegrationTests.Infrastructure;

namespace Warehouse.Api.IntegrationTests.Fixtures;

public class SagaFixture
{
    public ITestHarness Harness { get; private set; }

    public SagaFixture()
    {
        var config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .Build();

        StaticDependencyInjection.ServiceCollection
            .AddMassTransitTestHarness(cfg =>
        {
            cfg.SetMongoDbSagaRepositoryProvider(configurator =>
            {
                configurator.Connection = config.GetSection("MongoDb:ConnectionString").Value;
                configurator.DatabaseName = config.GetSection("MongoDb:DatabaseName").Value;
            });

            cfg.AddSagaStateMachine<ReviewOrderWorkflow, OrderState>(saga =>
            {

            }).MongoDbRepository(configurator =>
            {
                configurator.Connection = config.GetSection("MongoDb:ConnectionString").Value;
                configurator.DatabaseName = config.GetSection("MongoDb:DatabaseName").Value;
                configurator.CollectionName = config.GetSection("MongoDb:CollectionName").Value;
            }); ;

            cfg.AddSagaRepository<OrderState>();
        }).BuildServiceProvider(true);

        Harness = StaticDependencyInjection.ServiceProvider.GetRequiredService<ITestHarness>();
        Harness.Start().GetAwaiter().GetResult();
        Task.Delay(1000).GetAwaiter().GetResult();
    }
}