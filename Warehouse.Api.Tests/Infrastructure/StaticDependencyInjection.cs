﻿using Microsoft.Extensions.DependencyInjection;

namespace Warehouse.Api.IntegrationTests.Infrastructure;
public class StaticDependencyInjection
{
    public static IServiceCollection ServiceCollection { get; private set; } = CreateServiceCollection();
    public static IServiceProvider ServiceProvider => _serviceProvider.Value;

    private static Lazy<IServiceProvider> _serviceProvider = new(() => ServiceCollection.BuildServiceProvider());

    public static void Clean()
    {
        if (ServiceProvider is IDisposable disposable)
            disposable.Dispose();

        ServiceCollection.Clear();
        _serviceProvider = new(() => ServiceCollection.BuildServiceProvider());
    }

    private static IServiceCollection CreateServiceCollection()
    {
        var services = new ServiceCollection();
        return services;
    }
}
