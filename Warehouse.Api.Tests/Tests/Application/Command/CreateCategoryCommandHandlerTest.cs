﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Forex.CaseApi.Infrastructure;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute.ExceptionExtensions;
using Warehouse.Api.Application.Commands.Categories.Create;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Application.HttpModels;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;
using Warehouse.Api.Infrastructure.Repositories;
using Warehouse.Api.IntegrationTests.Fixtures;
using Xunit;

namespace Warehouse.Api.IntegrationTests.Tests.Application.Command
{
    public class CreateCategoryCommandHandlerTest : IClassFixture<MongoDbFixture>
    {
        public CreateCategoryCommandHandlerTest(MongoDbFixture mongoDbFixture)
        {
            MongoDbFixture = mongoDbFixture;

            ServiceProvider = new ServiceCollection()
                .AddSingleton(MongoDbFixture.MongoDbContext)
                .AddScoped(typeof(ICategoriesRepository), typeof(CategoriesRepository))
                .AddScoped(typeof(IIntSequenceGenerator<>), typeof(IntSequenceGenerator<>))
                .BuildServiceProvider();
        }

        private IServiceProvider ServiceProvider { get; }
        private MongoDbFixture MongoDbFixture { get; }

        public CreateCategoryCommandHandler SutInstance
            => ActivatorUtilities.CreateInstance<CreateCategoryCommandHandler>(ServiceProvider);

        [Theory]
        [AutoData]
        public async Task Command_Create_Category_If_Not_Exists(CreateCategoryRequest request)
        {
            // arrange
            var command = new CreateCategoryCommand(request);
            AutoFixture.Fixture fx = new AutoFixture.Fixture();
            var category = new Category(1, request.Name, request.LowStockThreshold, request.OutOfStockThreshold);

            // act
            await SutInstance.Handle(command, CancellationToken.None);

            // assert
            var result = (await MongoDbFixture.FindAsync<Category>(x => x.Name.Equals(request.Name))).ToList();


            result.Should().NotBeNull();
            result.Should().HaveCount(1)
                .And.Subject.First().Should().
                BeEquivalentTo(category, opt => opt.Excluding(x=>x.Id));
        }

        [Theory]
        [AutoData]
        public async Task Command_Should_Throw_Category_Exists_Exception(CreateCategoryRequest request)
        {
            // arrange
            await MongoDbFixture.PurgeAsync<Category>();
            var command = new CreateCategoryCommand(request);
            await MongoDbFixture.InsertAsync(new Category(1, request.Name, request.LowStockThreshold, request.OutOfStockThreshold));

            // act 
            var act = () => SutInstance.Handle(command, CancellationToken.None); 

            //assert
            await act.Should().ThrowAsync<CategoryExistsException>();
        }
    }
}
