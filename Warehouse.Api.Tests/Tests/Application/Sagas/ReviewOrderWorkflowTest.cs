﻿using AutoFixture.Xunit2;
using FluentAssertions;
using MassTransit;
using MassTransit.Testing;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.IntegrationTests.Fixtures;
using Xunit;

namespace Warehouse.Api.IntegrationTests.Tests.Application.Sagas
{
    public class ReviewOrderWorkflowTest : IClassFixture<SagaFixture>
    {
        private SagaFixture SagaFixture { get; set; }


        public ReviewOrderWorkflowTest(SagaFixture sagaFixture)
        {
            SagaFixture = sagaFixture;
        }

        [Theory]
        [AutoData]
        public async Task Create_New_Order_Event_Publish_Should_Publish_Review_Order_Event(Guid correlationId,
            CreateNewOrderEvent createOrderEvent)
        {
            // arrange
            createOrderEvent.CreateType = CreateOrderType.ToReview;
            createOrderEvent.CorrelationId = correlationId;

            // act
            await SagaFixture.Harness.Bus.Publish(createOrderEvent);
            await Task.Delay(500);

            // assert
            var sagaStateMachine = SagaFixture.Harness.GetSagaStateMachineHarness<ReviewOrderWorkflow, OrderState>();

            var createOrderIsPublished = await SagaFixture.Harness
                .Published.Any<CreateNewOrderEvent>(x => x.Context.Message.CorrelationId == correlationId);
            createOrderIsPublished.Should().BeTrue();

            sagaStateMachine.Consumed.Count().Should().Be(1);

            sagaStateMachine.Created.ContainsInState(correlationId,
                sagaStateMachine.StateMachine,
                sagaStateMachine.StateMachine.ReviewInProgress);

            var reviewOrderIsPublished = await SagaFixture.Harness
                .Published.Any<ReviewOrderEvent>(x => x.Context.Message.CorrelationId == correlationId);
            
            reviewOrderIsPublished.Should().BeTrue();
        }
    }
}
