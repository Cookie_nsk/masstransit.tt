using AutoFixture;
using AutoFixture.Xunit2;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using Warehouse.Api.Application.Queries.Categories;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.IntegrationTests.Infrastructure;
using Xunit;

namespace Warehouse.Api.IntegrationTests.Tests.Controllers
{
    public class ProductsControllerTests : IClassFixture<TestWebApplicationFactory<TestStartup>>
    {
        private readonly WebApplicationFactory<TestStartup> _webFactory;

        public ProductsControllerTests(TestWebApplicationFactory<TestStartup> testFactoryBase)
        {
            AutoFixture = new Fixture();
            _webFactory = testFactoryBase.WithWebHostBuilder(c =>
            {
                c.ConfigureServices(c =>
                {
                    c.AddSingleton(Mediator);
                });
            });
        }

        private IMediator Mediator = Substitute.For<IMediator>();

        public IFixture AutoFixture { get; }

        [Theory]
        [AutoData]
        public async Task Get_Should_Return_200_Ok(Product product)
        {
            // arrange
            var client = _webFactory.CreateClient();

            Mediator.Send(Arg.Is<GetProductQuery>(x => x.Id == product.Id),
                Arg.Any<CancellationToken>()).Returns(product);

            // act
            var response =
                await client.GetAsync($"{Endpoints.ProductsConfiguration.Get()}/{product.Id}", CancellationToken.None);

            // assert
            response.Should().Be200Ok();
            Mediator.ReceivedCalls().Count().Should().Be(1);
        }

        [Theory]
        [AutoData]
        public async Task Get_Should_Return_404_Not_Found(Product product)
        {
            // arrange
            var client = _webFactory.CreateClient();

            Mediator.Send(Arg.Is<GetProductQuery>(x => x.Id == product.Id),
                Arg.Any<CancellationToken>()).ReturnsNull();

            // act
            var response =
                await client.GetAsync($"{Endpoints.ProductsConfiguration.Get()}/{product.Id}", CancellationToken.None);

            // assert
            response.Should().Be404NotFound();
            Mediator.ReceivedCalls().Count().Should().Be(1);
        }
    }
}