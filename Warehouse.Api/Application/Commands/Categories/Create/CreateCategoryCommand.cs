﻿using MediatR;
using Warehouse.Api.Application.HttpModels;

namespace Warehouse.Api.Application.Commands.Categories.Create
{
    public class CreateCategoryCommand : IRequest<Unit>
    {
        public string Name { get; private set; }

        public int LowStockThreshold { get; private set; }

        public int OutOfStockThreshold { get; private set; }

        public CreateCategoryCommand(CreateCategoryRequest request)
        {
            Name = request.Name;
            LowStockThreshold = request.LowStockThreshold;
            OutOfStockThreshold = request.OutOfStockThreshold;
        }
    }
}
