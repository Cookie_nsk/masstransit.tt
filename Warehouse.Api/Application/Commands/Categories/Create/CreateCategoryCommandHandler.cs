﻿using Forex.CaseApi.Infrastructure;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Commands.Categories.Create
{
    public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand>
    {
        private readonly ICategoriesRepository _сategoriesRepository;
        private readonly IIntSequenceGenerator<Category> _idGenerator;
        public CreateCategoryCommandHandler(ICategoriesRepository categoriesRepository,
            IIntSequenceGenerator<Category> idGenerator)
        {
            _сategoriesRepository = categoriesRepository;
            _idGenerator = idGenerator;
        }

        public async Task<Unit> Handle(CreateCategoryCommand command, CancellationToken cancellationToken)
        {
            var category = await _сategoriesRepository.GetByNameAsync(command.Name, cancellationToken);

            if (category != null)
                throw new CategoryExistsException();

            await _сategoriesRepository.AddAsync(new Category(await _idGenerator.NextAsync(cancellationToken),
                command.Name, command.LowStockThreshold,
                command.OutOfStockThreshold), cancellationToken);

            return Unit.Value;
        }
    }
}
