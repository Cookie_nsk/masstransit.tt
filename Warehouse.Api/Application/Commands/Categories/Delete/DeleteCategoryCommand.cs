﻿using MediatR;

namespace Warehouse.Api.Application.Commands.Categories.Delete
{
    public class DeleteCategoryCommand : IRequest<Unit>
    {
        public int Id { get; private set; }

        public DeleteCategoryCommand(int id)
        {
            Id = id;
        }
    }
}
