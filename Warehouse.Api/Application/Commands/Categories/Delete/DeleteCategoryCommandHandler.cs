﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Commands.Categories.Delete
{
    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand>
    {
        private readonly ICategoriesRepository _сategoriesRepository;
        private readonly IProductsRepository _productsRepository;

        public DeleteCategoryCommandHandler(ICategoriesRepository categoriesRepository,
            IProductsRepository productsRepository)
        {
            _сategoriesRepository = categoriesRepository;
            _productsRepository = productsRepository;
        }

        public async Task<Unit> Handle(DeleteCategoryCommand command, CancellationToken cancellationToken)
        {
            if (await _сategoriesRepository.GetAsync(command.Id, cancellationToken) == null)
                throw new CategoryNotFoundException();

            if (await _productsRepository.GetLinkedProductsCountByCategory(command.Id, cancellationToken) > 0)
                throw new CategoryInUseException();

            await _сategoriesRepository.DeleteAsync(command.Id, cancellationToken);
            return Unit.Value;
        }
    }
}
