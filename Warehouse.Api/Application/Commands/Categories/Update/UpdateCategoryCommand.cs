﻿using MediatR;
using Warehouse.Api.Application.HttpModels;

namespace Warehouse.Api.Application.Commands.Categories.Update
{
    public class UpdateCategoryCommand : IRequest<Unit>
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int LowStockThreshold { get; private set; }
        public int OutOfStockThreshold { get; private set; }
        public UpdateCategoryCommand(UpdateCategoryRequest request)
        {
            Id = request.Id;
            Name = request.Name;
            LowStockThreshold = request.LowStockThreshold;
            OutOfStockThreshold = request.OutOfStockThreshold;
        }
    }
}
