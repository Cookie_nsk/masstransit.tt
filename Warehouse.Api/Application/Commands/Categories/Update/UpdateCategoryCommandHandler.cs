﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Commands.Categories.Update
{
    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand>
    {
        private readonly ICategoriesRepository _сategoriesRepository;

        public UpdateCategoryCommandHandler(ICategoriesRepository categoriesRepository)
        {
            _сategoriesRepository = categoriesRepository;
        }

        public async Task<Unit> Handle(UpdateCategoryCommand command, CancellationToken cancellationToken)
        {
            if (await _сategoriesRepository.GetAsync(command.Id, cancellationToken) == null)
                throw new CategoryNotFoundException();

            await _сategoriesRepository.UpdateAsync(command.Id, command.Name, cancellationToken);
            return Unit.Value;
        }
    }
}
