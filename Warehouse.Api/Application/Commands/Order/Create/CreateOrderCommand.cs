﻿using MediatR;
using Warehouse.Api.Domain.Enums;

namespace Warehouse.Api.Application.Commands.Order.Create
{
    public class CreateOrderCommand : IRequest<Unit>
    {
        public int ProductId { get; private set; }

        public int Quantity { get; private set; }

        public ReserveModeEnum ReserveMode { get; private set; }

        public CreateOrderCommand(int productId, int quantity, ReserveModeEnum reserveMode)
        {
            ProductId = productId;
            Quantity = quantity;
            ReserveMode = reserveMode;
        }
    }
}
