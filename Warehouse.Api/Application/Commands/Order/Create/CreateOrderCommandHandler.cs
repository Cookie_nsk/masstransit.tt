﻿using Forex.CaseApi.Infrastructure;
using MassTransit;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Application.Services.Abstractions;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Enums;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Commands.Order.Create
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand>
    {
        private readonly IProductsRepository _productsRepository;
        private readonly IOrdersRepository _ordersRepository;
        private readonly IOrdersStatePublishingService _ordersService;
        private readonly IIntSequenceGenerator<Domain.Entities.Order> _idGenerator;

        public CreateOrderCommandHandler(IProductsRepository productsRepository,
            IOrdersRepository ordersRepository, IOrdersStatePublishingService ordersService,
            IIntSequenceGenerator<Domain.Entities.Order> idGenerator)
        {
            _productsRepository = productsRepository;
            _ordersRepository = ordersRepository;
            _ordersService = ordersService;
            _idGenerator = idGenerator;
        }

        public async Task<Unit> Handle(CreateOrderCommand command, CancellationToken cancellationToken)
        {
            var product = await _productsRepository.GetWithCategoryAsync(command.ProductId, cancellationToken);

            if (product == null)
                throw new ProductNotFoundException();

            await ReserveItemsAsync(product, command.Quantity, command.ReserveMode, cancellationToken);

            return Unit.Value;
        }

        private async Task ReserveItemsAsync(Product product, int quantity,
            ReserveModeEnum reserveMode, CancellationToken cancellationToken)
        {
            switch (product.CalculateLeftovers(quantity))
            {
                case ThresholdStateEnum.Available:
                    await OrderProductAsync(product, quantity, reserveMode, OrderStateEnum.Approved, cancellationToken);
                    break;
                case ThresholdStateEnum.LowStock:
                    await OrderProductAsync(product, quantity, reserveMode, OrderStateEnum.ReviewInProgress, cancellationToken);
                    break;
                case ThresholdStateEnum.OutOfStock:
                    switch (reserveMode)
                    {
                        case ReserveModeEnum.WhenAvailable:
                            await CreateWaitingForStockOrderAsync(product, quantity, reserveMode, cancellationToken);
                            break;
                        case ReserveModeEnum.None:
                            throw new RequestedQuantityExceededException();
                    }
                    break;
            }
        }

        private async Task OrderProductAsync(Product product, int quantity,
            ReserveModeEnum reserveMode, OrderStateEnum orderState, CancellationToken cancellationToken)
        {
            var orderId = await _ordersRepository.AddAsync(new Domain.Entities.Order(
                await _idGenerator.NextAsync(cancellationToken),
                product.Id, quantity,
                orderState, reserveMode)
                , cancellationToken);

            bool retry = true;

            while (retry)
            {
                try
                {
                    product.ReserveItem(quantity);
                    await _productsRepository.UpdateAsync(product, cancellationToken);
                    retry = false;
                }
                catch (Exception exc) when (exc is InvalidOperationException || exc is ConcurrencyException)
                {//UpdatedAt is changing in ReserveItem()
                    product = await _productsRepository.GetWithCategoryAsync(product.Id, cancellationToken);
                    product.ReserveItem(quantity);
                    await Task.Delay(100);
                }
            }

            if (orderState == OrderStateEnum.ReviewInProgress)
                await _ordersService.PublishCreateOrderAsync(orderId, product.Id, quantity, CreateOrderType.ToReview);
        }

        private async Task CreateWaitingForStockOrderAsync(Product product, int quantity,
            ReserveModeEnum reserveMode, CancellationToken cancellationToken)
        {
            var guid = await _ordersRepository.AddAsync(new Domain.Entities.Order(
                await _idGenerator.NextAsync(cancellationToken),
                product.Id, quantity,
                OrderStateEnum.WaitingForStock, reserveMode)
                , cancellationToken);

            await _ordersService.PublishCreateOrderAsync(guid, product.Id, quantity, CreateOrderType.ToWaitingForStock);
        }
    }
}
