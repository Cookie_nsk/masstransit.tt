﻿using MediatR;
using Warehouse.Api.Application.HttpModels;

namespace Warehouse.Api.Application.Commands.Products.ChangeStock
{
    public class ChangeProductStockCommand : IRequest<Unit>
    {
        public int ProductId { get; private set; }

        public int Stock { get; private set; }

        public ChangeProductStockCommand(ChangeProductStockRequest request)
        {
            ProductId = request.ProductId;
            Stock = request.Stock;
        }
    }
}
