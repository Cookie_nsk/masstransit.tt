﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Application.Services.Abstractions;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Commands.Products.ChangeStock
{
    public class ChangeProductStockCommandHandler : IRequestHandler<ChangeProductStockCommand>
    {
        private readonly IProductsRepository _productRepository;
        private readonly IOrdersStatePublishingService _publishingService;

        public ChangeProductStockCommandHandler(IProductsRepository productRepository,
             IOrdersStatePublishingService publishingService)
        {
            _productRepository = productRepository;
            _publishingService = publishingService;
        }

        public async Task<Unit> Handle(ChangeProductStockCommand command, CancellationToken cancellationToken)
        {
            var product = await _productRepository.GetWithCategoryAsync(command.ProductId, cancellationToken);

            if (product == null)
                throw new ProductNotFoundException();

            product.SetStock(command.Stock);
            await _productRepository.UpdateStockAsync(product, cancellationToken);
            //Publishing stock changed
            await _publishingService.SendStockChangedAsync(product.Id);

            return Unit.Value;
        }

    }
}

