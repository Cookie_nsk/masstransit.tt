﻿using MediatR;
using Warehouse.Api.Application.HttpModels;

namespace Warehouse.Api.Application.Commands.Products.Create
{
    public class CreateProductCommand : IRequest<Unit>
    {
        public string Name { get; private set; }
        public int CategoryId { get; private set; }
        public int Stock { get; private set; }

        public CreateProductCommand(CreateProductRequest createRequest)
        {
            Name = createRequest.Name;
            CategoryId = createRequest.CategoryId;
            Stock = createRequest.Stock;
        }
    }
}
