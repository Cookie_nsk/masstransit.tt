﻿using Forex.CaseApi.Infrastructure;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Commands.Products.Create
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand>
    {
        private readonly IProductsRepository _productRepository;
        private readonly ICategoriesRepository _categoriesRepository;
        private readonly IIntSequenceGenerator<Product> _idGenerator;

        public CreateProductCommandHandler(IProductsRepository productRepository,
            ICategoriesRepository categoriesRepository, IIntSequenceGenerator<Product> idGenerator)
        {
            _productRepository = productRepository;
            _categoriesRepository = categoriesRepository;
            _idGenerator = idGenerator;
        }

        public async Task<Unit> Handle(CreateProductCommand command, CancellationToken cancellationToken)
        {
            if (await _categoriesRepository.GetAsync(command.CategoryId, cancellationToken) == null)
                throw new CategoryNotFoundException();

            if (await _productRepository.GetCountByNameAsync(command.Name, cancellationToken) > 0)
                throw new ProductExistsException();

            await _productRepository.AddAsync(new Product(await _idGenerator.NextAsync(cancellationToken),
                command.Name, command.Stock,
                command.CategoryId), cancellationToken);

            return Unit.Value;
        }
    }
}
