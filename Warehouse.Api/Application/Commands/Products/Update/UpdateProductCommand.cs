﻿using MediatR;
using Warehouse.Api.Application.HttpModels;

namespace Warehouse.Api.Application.Commands.Products.Update
{
    public class UpdateProductCommand : IRequest<Unit>
    {
        public int Id { get; private set; }

        public string Name { get; private set; }

        public int CategoryId { get; private set; }

        public int Stock { get; private set; }

        public UpdateProductCommand(UpdateProductRequest request)
        {
            Id = request.Id;
            Name = request.Name;
            CategoryId = request.CategoryId;
            Stock = request.Stock;
        }
    }
}
