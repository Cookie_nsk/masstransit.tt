﻿using MassTransit;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Commands.Products.Update
{
    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand>
    {
        private readonly IProductsRepository _productsRepository;
        private readonly ICategoriesRepository _сategoriesRepository;

        public UpdateProductCommandHandler(IProductsRepository productsRepository,
            ICategoriesRepository сategoriesRepository)
        {
            _productsRepository = productsRepository;
            _сategoriesRepository = сategoriesRepository;
        }

        public async Task<Unit> Handle(UpdateProductCommand command, CancellationToken cancellationToken)
        {
            bool retry = true;

            while (retry)
            {
                try
                {
                    var product = await _productsRepository.GetWithCategoryAsync(command.Id, cancellationToken);

                    if (product == null)
                        throw new ProductNotFoundException();

                    if (await _сategoriesRepository.GetAsync(command.CategoryId, cancellationToken) == null)
                        throw new CategoryNotFoundException();

                    product.Update(command.Name, command.Stock, command.CategoryId);

                    await _productsRepository.UpdateAsync(product, cancellationToken);
                    retry = false;
                }
                catch (Exception exc) when (exc is InvalidOperationException || exc is ConcurrencyException)
                {
                    await Task.Delay(100);
                }
            }

            return Unit.Value;
        }
    }
}
