﻿using MassTransit;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Consumers.EventHandlers
{
    public class ApproveOrderEventHandler : IConsumer<ApproveOrderCommand>
    {
        private readonly IOrdersRepository _ordersRepository;

        public ApproveOrderEventHandler(IOrdersRepository ordersRepository)
        {
            _ordersRepository = ordersRepository;
        }

        public async Task Consume(ConsumeContext<ApproveOrderCommand> context)
        {
            var order = await _ordersRepository.GetAsync(context.Message.OrderId, CancellationToken.None);
            order.SetStateApproved();
            await _ordersRepository.UpdateAsync(order, CancellationToken.None);
        }
    }
}
