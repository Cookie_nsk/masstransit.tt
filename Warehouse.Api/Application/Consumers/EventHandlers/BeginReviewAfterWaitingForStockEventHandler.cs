﻿using MassTransit;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Application.Services.Abstractions;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Consumers.EventHandlers
{
    public class BeginReviewAfterWaitingForStockEventHandler : IConsumer<ReviewAfterWaitingForStockEvent>
    {
        private readonly IProductsRepository _productsRepository;
        private readonly IOrdersRepository _ordersRepository;
        private readonly IOrdersStatePublishingService _ordersService;

        public BeginReviewAfterWaitingForStockEventHandler(IProductsRepository productRepository,
            IOrdersRepository ordersRepository, IOrdersStatePublishingService ordersService)
        {
            _productsRepository = productRepository;
            _ordersRepository = ordersRepository;
            _ordersService = ordersService;
        }

        public async Task Consume(ConsumeContext<ReviewAfterWaitingForStockEvent> context)
        {
            var cancellationToken = CancellationToken.None;

            var order = await _ordersRepository.GetAsync(context.Message.OrderId, cancellationToken);
            var product = await _productsRepository.GetWithCategoryAsync(order.ProductId, cancellationToken);
            
            order.SetStateReviewInProgress();
            product.ReserveItem(order.Quantity);

            await _ordersRepository.UpdateAsync(order, cancellationToken);
            await _productsRepository.UpdateAsync(product, cancellationToken);

            await _ordersService.SendReviewOrderAsync(order.Id, context.Message.CorrelationId);
        }
    }
}
