﻿using MassTransit;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Consumers.EventHandlers
{
    public class BeginUpdateOrderEventHandler : IConsumer<UpdateOrderEvent>
    {
        private readonly IProductsRepository _productsRepository;
        private readonly IOrdersRepository _ordersRepository;

        public BeginUpdateOrderEventHandler(IProductsRepository productRepository,
            IOrdersRepository ordersRepository)
        {
            _productsRepository = productRepository;
            _ordersRepository = ordersRepository;
        }

        public async Task Consume(ConsumeContext<UpdateOrderEvent> context)
        {
            var cancellationToken = CancellationToken.None;

            var order = await _ordersRepository.GetAsync(context.Message.OrderId, cancellationToken);
            var product = await _productsRepository.GetWithCategoryAsync(order.ProductId, cancellationToken);

            product.ReserveItem(order.Quantity);
            order.SetStateApproved();

            await _ordersRepository.UpdateAsync(order, cancellationToken);
            await _productsRepository.UpdateAsync(product, cancellationToken);
        }
    }
}
