﻿using MassTransit;
using System;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Application.Services.Abstractions;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Consumers.EventHandlers
{
    public class DeclineOrderEventHandler : IConsumer<DeclineOrderCommand>
    {
        private readonly IOrdersRepository _ordersRepository;
        private readonly IProductsRepository _productsRepository;
        private readonly IOrdersStatePublishingService _publishingService;

        public DeclineOrderEventHandler(IOrdersRepository ordersRepository,
            IProductsRepository productsRepository, IOrdersStatePublishingService publishingService)
        {
            _ordersRepository = ordersRepository;
            _productsRepository = productsRepository;
            _publishingService = publishingService;
        }

        public async Task Consume(ConsumeContext<DeclineOrderCommand> context)
        {
            var order = await _ordersRepository.GetAsync(context.Message.OrderId, CancellationToken.None);
            order.SetStateDeclined();
            await _ordersRepository.UpdateAsync(order, CancellationToken.None);

            var product = await _productsRepository.GetWithCategoryAsync(order.ProductId, CancellationToken.None);
            product.ReturnItemsToStock(order.Quantity);
            await _productsRepository.UpdateAsync(product, CancellationToken.None);
            //Publishing stock changed
            await _publishingService.SendStockChangedAsync(product.Id);

        }
    }
}
