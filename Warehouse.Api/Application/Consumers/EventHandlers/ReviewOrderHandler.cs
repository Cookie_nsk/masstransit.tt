﻿using MassTransit;
using System.Threading.Tasks;
using System.Threading;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Domain.Repositories.Abstractions;
using System;

namespace Warehouse.Api.Application.Consumers.EventHandlers
{
    public class ReviewOrderHandler : IConsumer<ReviewOrderEvent>
    {
        public ReviewOrderHandler()
        {
        }

        public async Task Consume(ConsumeContext<ReviewOrderEvent> context)
        {
            Console.WriteLine($"Order review request received. Order.Id = {context.Message.OrderId}");

            Random rnd = new Random();

            if (/*rnd.Next(100) % 2 == 0*/true)
            {
                Console.WriteLine($"    Order state set to approved. Order.Id = {context.Message.OrderId}");
                await context.Publish(new OrderApprovedEvent()
                {
                    OrderId = context.Message.OrderId,
                    CorrelationId = context.Message.CorrelationId
                });
            }
            else
            {
                Console.WriteLine($"Order state set to declined. Order.Id = {context.Message.OrderId}");
                await context.Publish(new OrderDeclinedEvent()
                {
                    OrderId = context.Message.OrderId,
                    CorrelationId = context.Message.CorrelationId
                });
            }
        }
    }
}
