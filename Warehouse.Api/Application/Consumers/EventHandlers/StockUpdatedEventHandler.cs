﻿using MassTransit;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Application.Services.Abstractions;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Enums;
using Warehouse.Api.Domain.Exceptions;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Consumers.EventHandlers
{
    public class StockUpdatedEventHandler : IConsumer<StockUpdatedEvent>
    {
        private readonly IProductsRepository _productsRepository;
        private readonly IOrderStateRepository _orderStateRepository;
        private readonly IOrdersStatePublishingService _publishingService;
        private readonly IOrdersRepository _ordersRepository;

        public StockUpdatedEventHandler(IProductsRepository productRepository,
            IOrderStateRepository orderStateRepository, IOrdersStatePublishingService publishingService,
            IOrdersRepository ordersRepository)
        {
            _productsRepository = productRepository;
            _orderStateRepository = orderStateRepository;
            _publishingService = publishingService;
            _ordersRepository = ordersRepository;
        }

        public async Task Consume(ConsumeContext<StockUpdatedEvent> context)
        {
            var product = await _productsRepository.GetWithCategoryAsync(context.Message.ProductId, CancellationToken.None);

            var orderStates = (await _orderStateRepository
            .GetWaitingForStockByParamsAsync(product.Id, product.Stock, CancellationToken.None))
            .OrderBy(x => x.CreatedAt).ToList();

            while (product.Stock > 0 && orderStates.Count(x => x.Quantity <= product.Stock) > 0)
            {
                var orderState = orderStates.First();
                await ProcessPendingOrders(product, orderState.OrderId, orderState.CorrelationId);
                orderStates.Remove(orderState);
            }
        }

        private async Task ProcessPendingOrders(Product product, int orderId, Guid correlationId)
        {//
            var order = await _ordersRepository.GetAsync(orderId, CancellationToken.None);

            if (product.Stock - order.Quantity < 0)
                throw new ProductOutOfStockException();

            switch (product.CalculateLeftovers(order.Quantity))
            {
                case ThresholdStateEnum.Available:
                    await _publishingService.SendUpdateOrderAsync(order.Id, correlationId);
                    break;
                case ThresholdStateEnum.LowStock or ThresholdStateEnum.OutOfStock:
                    await _publishingService.SendReviewAfterWaitingForStockAsync(order.Id, correlationId);
                    break;
            }
        }
    }
}