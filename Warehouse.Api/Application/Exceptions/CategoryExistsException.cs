﻿using System;

namespace Warehouse.Api.Application.Exceptions
{
    public class CategoryExistsException : Exception
    {
        public CategoryExistsException() : base("Category with such name already exists, pick another one")
        { }
    }
}
