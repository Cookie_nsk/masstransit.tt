﻿using System;

namespace Warehouse.Api.Application.Exceptions
{
    public class CategoryInUseException : Exception
    {
        public CategoryInUseException() : base("Category in use. Remove category from all Orders and try again.")
        { }
    }
}
