﻿using System;

namespace Warehouse.Api.Application.Exceptions
{
    public class CategoryNotFoundException : Exception
    {
        public CategoryNotFoundException() : base("Category not found")
        { }
    }
}
