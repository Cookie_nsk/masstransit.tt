﻿using System;

namespace Warehouse.Api.Application.Exceptions
{
    public class ProductExistsException : Exception
    {
        public ProductExistsException() : base("Product with such name already exists")
        { }
    }
}
