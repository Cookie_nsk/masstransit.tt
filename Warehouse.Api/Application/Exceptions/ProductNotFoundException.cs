﻿using System;

namespace Warehouse.Api.Application.Exceptions
{
    public class ProductNotFoundException : Exception
    {
        public ProductNotFoundException() : base("Product not found")
        { }
    }
}
