﻿using System;

namespace Warehouse.Api.Application.Exceptions
{
    public class RequestedQuantityExceededException : Exception
    {
        public RequestedQuantityExceededException() : base("Requested quantity is more than available in stock")
        { }
    }
}
