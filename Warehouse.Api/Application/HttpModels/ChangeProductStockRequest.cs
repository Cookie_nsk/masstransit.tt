﻿namespace Warehouse.Api.Application.HttpModels
{
    public class ChangeProductStockRequest
    {
        public int ProductId { get; set; }

        public int Stock { get; set; }
    }
}
