﻿namespace Warehouse.Api.Application.HttpModels
{
    public class CreateCategoryRequest
    {
        public string Name { get; set; }
        public int LowStockThreshold { get; set; }
        public int OutOfStockThreshold { get; set; }
    }
}
