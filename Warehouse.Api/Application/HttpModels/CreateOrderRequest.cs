﻿using Warehouse.Api.Domain.Enums;

namespace Warehouse.Api.Application.HttpModels
{
    public class CreateOrderRequest
    {
        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public ReserveModeEnum ReserveMode { get; set; }
    }
}
