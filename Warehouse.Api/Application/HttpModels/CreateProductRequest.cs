﻿namespace Warehouse.Api.Application.HttpModels
{
    public class CreateProductRequest
    {
        public string Name { get; set; }

        public int CategoryId { get; set; }

        public int Stock { get; set; }
    }
}
