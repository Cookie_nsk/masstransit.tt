﻿namespace Warehouse.Api.Application.HttpModels
{
    public class UpdateProductRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int CategoryId { get; set; }

        public int Stock { get; set; }
    }
}
