﻿using MediatR;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Application.Queries.Categories
{
    public class GetCategoryQuery : IRequest<Category>
    {
        public int Id { get; private set; }

        public GetCategoryQuery(int id)
        {
            Id = id;
        }
    }
}
