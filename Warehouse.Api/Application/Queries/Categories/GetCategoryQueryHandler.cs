﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Queries.Categories
{
    public class GetCategoryQueryHandler : IRequestHandler<GetCategoryQuery, Category>
    {
        private readonly ICategoriesRepository _сategoriesRepository;

        public GetCategoryQueryHandler(ICategoriesRepository categoriesRepository)
        {
            _сategoriesRepository = categoriesRepository;
        }

        public async Task<Category> Handle(GetCategoryQuery command, CancellationToken cancellationToken)
        {
            return await _сategoriesRepository.GetAsync(command.Id, cancellationToken);
        }
    }
}
