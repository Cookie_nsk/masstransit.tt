﻿using MediatR;
using System.Collections.Generic;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Application.Queries.Categories
{
    public class GetOrderReportQuery : IRequest<IEnumerable<OrderDto>>
    { }
}
