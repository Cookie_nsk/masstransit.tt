﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Queries.Categories
{
    public class GetOrderReportQueryHandler : IRequestHandler<GetOrderReportQuery, IEnumerable<OrderDto>>
    {
        private readonly IOrdersRepository _ordersRepository;

        public GetOrderReportQueryHandler(IOrdersRepository ordersRepository)
        {
            _ordersRepository = ordersRepository;
        }

        public async Task<IEnumerable<OrderDto>> Handle(GetOrderReportQuery request, CancellationToken cancellationToken)
        {
            return await _ordersRepository.GetReportAsync(cancellationToken);
        }
    }
}
