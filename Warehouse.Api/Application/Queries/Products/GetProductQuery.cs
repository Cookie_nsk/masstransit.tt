﻿using MediatR;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Application.Queries.Categories
{
    public class GetProductQuery : IRequest<Product>
    {
        public int Id { get; private set; }

        public GetProductQuery(int id)
        {
            Id = id;
        }
    }
}
