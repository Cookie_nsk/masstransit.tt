﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Application.Queries.Categories
{
    public class GetProductQueryHandler : IRequestHandler<GetProductQuery, Product>
    {
        private readonly IProductsRepository _productRepository;

        public GetProductQueryHandler(IProductsRepository categoriesRepository)
        {
            _productRepository = categoriesRepository;
        }

        public async Task<Product> Handle(GetProductQuery command, CancellationToken cancellationToken)
        {
            return await _productRepository.GetWithCategoryAsync(command.Id, cancellationToken);
        }
    }
}
