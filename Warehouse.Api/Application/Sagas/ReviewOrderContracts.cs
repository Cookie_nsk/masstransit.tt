﻿using MassTransit;
using System;

namespace Warehouse.Api.Application.Sagas
{
    public enum CreateOrderType
    {
        ToReview,
        ToWaitingForStock
    }

    public class CreateNewOrderEvent : CorrelatedBy<Guid>
    {
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
        public Guid CorrelationId { get; set; }
        public CreateOrderType CreateType { get; set; }
    }
    public class StockUpdatedEvent
    {
        public int ProductId { get; set; }
        public Guid CorrelationId { get; set; }
    }
    public class ReviewOrderEvent
    {
        public int OrderId { get; set; }
        public Guid CorrelationId { get; set; }
    }
    public class UpdateOrderEvent : CorrelatedBy<Guid>
    {
        public int OrderId { get; set; }
        public Guid CorrelationId { get; set; }
    }
    public class ReviewAfterWaitingForStockEvent : CorrelatedBy<Guid>
    {
        public int OrderId { get; set; }
        public Guid CorrelationId { get; set; }
    }
    public class ResumeAfterWaitingForStockCommand
    {
        public int ProductId { get; set; }
        public Guid CorrelationId { get; set; }
    }
    public class ApproveOrderCommand
    {
        public int OrderId { get; set; }
        public Guid CorrelationId { get; set; }
    }
    public class DeclineOrderCommand
    {
        public int OrderId { get; set; }

        public Guid CorrelationId { get; set; }
    }
    public class OrderApprovedEvent : CorrelatedBy<Guid>
    {
        public int OrderId { get; set; }
        public Guid CorrelationId { get; set; }
    }
    public class OrderDeclinedEvent : CorrelatedBy<Guid>
    {
        public int OrderId { get; set; }
        public Guid CorrelationId { get; set; }
    }

    
}
