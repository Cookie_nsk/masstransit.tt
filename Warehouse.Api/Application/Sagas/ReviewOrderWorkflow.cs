﻿using MassTransit;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Application.Sagas
{
    public class ReviewOrderWorkflow : MassTransitStateMachine<OrderState>
    {
        public Event<CreateNewOrderEvent> CreateNewOrderEvent { get; }

        public Event<ReviewOrderEvent> ReviewOrderEvent { get; }
        public Event<OrderApprovedEvent> OrderApprovedEvent { get; }
        public Event<OrderDeclinedEvent> OrderDeclinedEvent { get; }

        public Event<StockUpdatedEvent> StockUpdatedEvent { get; }
        public Event<ResumeAfterWaitingForStockCommand> ResumeAfterWaitingForStockCommand { get; }
        public Event<ReviewAfterWaitingForStockEvent> ReviewAfterWaitingForStockEvent { get; }
        public Event<ApproveOrderCommand> ApproveOrderCommand { get; }
        public Event<DeclineOrderCommand> DeclineOrderCommand { get; }

        public State ReviewInProgress { get; }
        public State WaitingForStock { get; }
        public State OrderApproved { get; }
        public State OrderDeclined { get; }
        public State ResumeAfterWaitingForStock { get; }
        public State ProcessTimeoutExpired { get; }

        public ReviewOrderWorkflow()
        {
            InstanceState(c => c.SagaState);

            Initially(
                When(CreateNewOrderEvent)
                .If(mess => mess.Message.CreateType == CreateOrderType.ToReview,
                    x => x.Publish(x => new ReviewOrderEvent() { OrderId = x.Message.OrderId, CorrelationId = x.Message.CorrelationId })
                    .Then(t =>
                    {
                        t.Saga.ProductId = t.Message.ProductId;
                        t.Saga.OrderId = t.Message.OrderId;
                    })
                    .TransitionTo(ReviewInProgress))

                .If(x => x.Message.CreateType == CreateOrderType.ToWaitingForStock,
                    x => x.Then(t =>
                    {
                        t.Saga.ProductId = t.Message.ProductId;
                        t.Saga.OrderId = t.Message.OrderId;
                    })
                    .TransitionTo(WaitingForStock)));


            DuringAny(Ignore(ReviewOrderEvent));
            DuringAny(Ignore(ResumeAfterWaitingForStockCommand));
            DuringAny(Ignore(ApproveOrderCommand));
            DuringAny(Ignore(DeclineOrderCommand));

            During(ReviewInProgress,
                When(OrderApprovedEvent)
                    .Publish(x => new ApproveOrderCommand() { OrderId = x.Message.OrderId, CorrelationId = x.Message.CorrelationId })
                .TransitionTo(OrderApproved));

            During(ReviewInProgress,
                When(OrderDeclinedEvent)
                .Publish(x => new DeclineOrderCommand() { OrderId = x.Message.OrderId, CorrelationId = x.Message.CorrelationId })
                .TransitionTo(OrderApproved));

            During(WaitingForStock,
                When(StockUpdatedEvent)
                .Publish(x => new ResumeAfterWaitingForStockCommand() { ProductId = x.Message.ProductId, CorrelationId = x.Message.CorrelationId })
                .TransitionTo(ResumeAfterWaitingForStock));

            During(ResumeAfterWaitingForStock,
                When(ReviewAfterWaitingForStockEvent)
                .Publish(x => new ReviewOrderEvent() { OrderId = x.Message.OrderId, CorrelationId = x.Message.CorrelationId })
                .TransitionTo(ReviewInProgress));

        }
    }
}
