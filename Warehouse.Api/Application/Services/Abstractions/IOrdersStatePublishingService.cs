﻿using System;
using System.Threading.Tasks;
using Warehouse.Api.Application.Sagas;

namespace Warehouse.Api.Application.Services.Abstractions
{
    public interface IOrdersStatePublishingService
    {
        Task PublishCreateOrderAsync(int orderId, int productId, int quantity, CreateOrderType createOrderType);
        Task SendReviewOrderAsync(int orderId, Guid correlationId);
        Task SendReviewAfterWaitingForStockAsync(int orderId, Guid correlationId);
        Task SendStockChangedAsync(int productId);
        Task SendUpdateOrderAsync(int orderId, Guid correlationId);
    }
}
