﻿using MassTransit;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Warehouse.Api.Application.Sagas;

namespace Warehouse.Api.Application.Services.Abstractions
{
    public class OrdersStatePublishingService : IOrdersStatePublishingService
    {
        private readonly IBus _busSender;
        private readonly ILogger<OrdersStatePublishingService> _logger;

        public OrdersStatePublishingService(IBus busSender,
            ILogger<OrdersStatePublishingService> logger)
        {
            _busSender = busSender;
            _logger = logger;
        }

        public async Task SendReviewAfterWaitingForStockAsync(int orderId, Guid correlationId)
        {
            _logger.LogInformation($"ReviewAfterWaitingForStockEvent was sent. Order.Id = {orderId}");
            await _busSender.Publish(new ReviewAfterWaitingForStockEvent() { OrderId = orderId, CorrelationId = correlationId });
        }
        
        public async Task SendStockChangedAsync(int productId)
        {
            _logger.LogInformation($"StockChangedEvent was sent. ProductId = {productId}");
            await _busSender.Publish(new StockUpdatedEvent() { ProductId = productId });
        }

        public async Task SendReviewOrderAsync(int orderId, Guid correlationId)
        {
            _logger.LogInformation($"ReviewOrderEvent was sent. Order.Id = {orderId}");
            await _busSender.Send(new ReviewOrderEvent() { OrderId = orderId, CorrelationId = correlationId });
        }

        public async Task SendUpdateOrderAsync(int orderId, Guid correlationId)
        {
            _logger.LogInformation($"UpdateOrderEvent was sent. Order.Id = {orderId}");
            await _busSender.Publish(new UpdateOrderEvent() { OrderId = orderId, CorrelationId = correlationId});
        }

        public async Task PublishCreateOrderAsync(int orderId, int productId, int quantity,
         CreateOrderType createOrderType)
        {
            _logger.LogInformation($"CreateNewOrderCommand was sent. Order.Id = {orderId}");
            await _busSender.Publish(new CreateNewOrderEvent()
            {
                OrderId = orderId,
                ProductId = productId,
                Quantity = quantity,
                CorrelationId = Guid.NewGuid(),
                CreateType = createOrderType
            });
        }
    }
}
