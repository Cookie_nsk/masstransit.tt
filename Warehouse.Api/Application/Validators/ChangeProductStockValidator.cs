﻿using FluentValidation;
using Warehouse.Api.Application.Commands.Products.ChangeStock;

namespace Warehouse.Api.Application.Validators
{
    public class ChangeProductStockValidator : AbstractValidator<ChangeProductStockCommand>
    {
        public ChangeProductStockValidator()
        {
            RuleFor(model => model.Stock).GreaterThan(-1)
                .WithMessage("Stock should be greater than -1");

            RuleFor(model => model.Stock).LessThan(int.MaxValue)
                .WithMessage($"Stock should be lesser than {int.MaxValue}");
        }
    }
}
