﻿using FluentValidation;
using Warehouse.Api.Application.Commands.Categories.Create;

namespace Warehouse.Api.Application.Validators
{
    public class CreateCategoryValidator : AbstractValidator<CreateCategoryCommand>
    {
        public CreateCategoryValidator()
        {
            RuleFor(model => model.Name).NotEmpty()
                    .WithMessage("Name should be not empty");

            RuleFor(model => model.Name).MaximumLength(64)
                    .WithMessage("Name lenght should be should be lesser than 64");

            RuleFor(model => model.OutOfStockThreshold).GreaterThan(-1)
                    .WithMessage("OutOfStockThreshold should be positive number or 0");

            RuleFor(model => model.OutOfStockThreshold).LessThan(int.MaxValue)
                    .WithMessage("OutOfStockThreshold should be lesser than {int.MaxValue}");

            RuleFor(model => model.LowStockThreshold).GreaterThan(0)
                    .WithMessage("LowStockThreshold should be greater than 0");

            RuleFor(model => model.LowStockThreshold).LessThan(int.MaxValue)
                .WithMessage($"Quantity should be lesser than {int.MaxValue}");
        }
    }
}
