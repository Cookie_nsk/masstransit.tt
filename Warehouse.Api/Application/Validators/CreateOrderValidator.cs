﻿using FluentValidation;
using Warehouse.Api.Application.Commands.Order.Create;

namespace Warehouse.Api.Application.Validators
{
    public class CreateOrderValidator : AbstractValidator<CreateOrderCommand>
    {
        public CreateOrderValidator()
        {

            RuleFor(model => model.Quantity).GreaterThan(0)
                .WithMessage("Quantity should be greater than 0");

            RuleFor(model => model.Quantity).LessThan(int.MaxValue)
                .WithMessage($"Quantity should be lesser than {int.MaxValue}");
        }
    }
}
