﻿using FluentValidation;
using Warehouse.Api.Application.Commands.Products.Create;

namespace Warehouse.Api.Application.Validators
{
    public class CreateProductValidator : AbstractValidator<CreateProductCommand>
    {
        public CreateProductValidator()
        {
            RuleFor(model => model.Name).NotEmpty()
                .MinimumLength(3).WithMessage("Minimum Name length is 3");

            RuleFor(model => model.Stock).GreaterThan(0)
                .WithMessage("Stock should be greater than 0");

            RuleFor(model => model.Stock).LessThan(int.MaxValue)
                .WithMessage($"Stock should be lesser than {int.MaxValue}");
        }
    }
}
