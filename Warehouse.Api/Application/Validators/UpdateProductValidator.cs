﻿using FluentValidation;
using Warehouse.Api.Application.Commands.Products.Update;

namespace Warehouse.Api.Application.Validators
{
    public class UpdateProductValidator : AbstractValidator<UpdateProductCommand>
    {
        public UpdateProductValidator()
        {
            RuleFor(model => model.Name).MinimumLength(3)
                .WithMessage("Minimum Name length is 3");

            RuleFor(model => model.Stock).GreaterThan(0)
                .WithMessage("Stock should be greater than 0");

            RuleFor(model => model.Stock).LessThan(int.MaxValue)
                .WithMessage($"Stock should be lesser than {int.MaxValue}");
        }
    }
}
