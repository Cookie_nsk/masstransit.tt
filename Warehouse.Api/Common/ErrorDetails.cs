﻿using System.Text.Json;

namespace Warehouse.Api.Common
{
    public class ErrorDetails
    {
        public int StatusCode { get; private set; }
        public string Message { get; private set; }

        public ErrorDetails(int statusCode = 500, string message = "Something went wrong. Internal server error")
        {
            StatusCode = statusCode;
            Message = message;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
