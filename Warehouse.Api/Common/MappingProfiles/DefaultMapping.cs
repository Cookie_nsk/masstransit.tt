﻿using AutoMapper;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Common
{
    public class DefaultMapping : Profile
    {
        public DefaultMapping()
        {
            CreateMap<Order, OrderDto>()
                .ForMember(x => x.ReserveMode, opt => opt.MapFrom(t => t.ReserveMode.ToString()))
                .ForMember(x => x.State, opt => opt.MapFrom(t => t.State.ToString()));

            CreateMap<Product, OrderDto>()
                .ForMember(x => x.Number, opt => opt.Ignore())
                .ForMember(x => x.ProductName, opt => opt.MapFrom(t => t.Name));
        }
    }
}
