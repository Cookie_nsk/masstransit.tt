﻿using DnsClient.Internal;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Warehouse.Api.Application.Exceptions;
using Warehouse.Api.Domain.Exceptions;

namespace Warehouse.Api.Common.Middleware
{
    public class GlobalExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<GlobalExceptionMiddleware> _logger;

        public GlobalExceptionMiddleware(RequestDelegate next, ILogger<GlobalExceptionMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong: {ex}");
                await HandleExceptionAsync(httpContext, ex);
            }
        }
        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";

            if (exception is ValidationException)
            {
                await HandleValidationExceptions(context, exception);
            }
            else
            {
                var errorDetails = GetErrorDetails(context, exception);
                await context.Response.WriteAsync(errorDetails.ToString());
            }
        }
        private async Task HandleValidationExceptions(HttpContext context, Exception exc)
        {
            var errors = new List<ErrorDetails>();
            foreach (var error in (exc as ValidationException).Errors)
            {
                errors.Add(new ErrorDetails(StatusCodes.Status400BadRequest, error.ErrorMessage));
            }
            await context.Response.WriteAsync(JsonSerializer.Serialize(errors));
        }

        private ErrorDetails GetErrorDetails(HttpContext context, Exception exception)
        {
            context.Response.StatusCode = GetStatusCode(exception);
            return new ErrorDetails(context.Response.StatusCode, exception.Message);
        }
        private int GetStatusCode(Exception exception)
        {
            return exception switch
            {
                CategoryExistsException or
                CategoryInUseException or
                RequestedQuantityExceededException or
                ProductExistsException or
                InvalidOrderStateException or
                ProductOutOfStockException or
                RequestedQuantityExceededException => StatusCodes.Status400BadRequest,
                CategoryNotFoundException or
                ProductNotFoundException => StatusCodes.Status404NotFound,
                _ => StatusCodes.Status500InternalServerError
            };
        }

    }
}
