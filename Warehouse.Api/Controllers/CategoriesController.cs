﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Commands.Categories.Create;
using Warehouse.Api.Application.Commands.Categories.Delete;
using Warehouse.Api.Application.Commands.Categories.Update;
using Warehouse.Api.Application.HttpModels;
using Warehouse.Api.Application.Queries.Categories;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CategoriesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Category), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAsync([FromQuery] int id, CancellationToken cancellationToken)
        {
            var category = await _mediator.Send(new GetCategoryQuery(id), cancellationToken);
            return category == null ? NotFound() : Ok(category);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync([FromBody] CreateCategoryRequest request,
            CancellationToken cancellationToken)
        {
            await _mediator.Send(new CreateCategoryCommand(request), cancellationToken);
            return Ok();
        }

        [HttpPatch]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateAsync([FromBody] UpdateCategoryRequest request,
            CancellationToken cancellationToken)
        {
            await _mediator.Send(new UpdateCategoryCommand(request), cancellationToken);
            return Ok();
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteAsync([FromQuery] int id, CancellationToken cancellationToken)
        {
            await _mediator.Send(new DeleteCategoryCommand(id), cancellationToken);
            return Ok();
        }
    }
}
