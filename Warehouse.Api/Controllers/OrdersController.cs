﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Warehouse.Api.Application.Commands.Order.Create;
using Warehouse.Api.Application.HttpModels;
using Warehouse.Api.Application.Queries.Categories;
using Warehouse.Api.Domain.Entities;

namespace WarehouseApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrdersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("history")]
        [ProducesResponseType(typeof(IEnumerable<OrderDto>), StatusCodes.Status200OK)]
        public async Task<IEnumerable<OrderDto>> HistoryAsync()
        {
            return await _mediator.Send(new GetOrderReportQuery());
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync([FromBody] CreateOrderRequest request)
        {
            await _mediator.Send(new CreateOrderCommand(request.ProductId, request.Quantity, request.ReserveMode));
            return Ok();
        }
    }
}
