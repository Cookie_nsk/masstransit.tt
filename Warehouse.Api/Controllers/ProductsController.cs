﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Application.Commands.Products.ChangeStock;
using Warehouse.Api.Application.Commands.Products.Create;
using Warehouse.Api.Application.Commands.Products.Update;
using Warehouse.Api.Application.HttpModels;
using Warehouse.Api.Application.Queries.Categories;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Product), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAsync([FromRoute] int id, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(new GetProductQuery(id), cancellationToken);
            return result == null ? NotFound() : Ok(result);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync([FromBody] CreateProductRequest request,
            CancellationToken cancellationToken)
        {
            await _mediator.Send(new CreateProductCommand(request), cancellationToken);
            return Ok();
        }

        [HttpPatch]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateAsync([FromBody] UpdateProductRequest request,
            CancellationToken cancellationToken)
        {
            await _mediator.Send(new UpdateProductCommand(request), cancellationToken);
            return Ok();
        }

        [HttpPut("changeStock")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ChangeStockAsync([FromBody] ChangeProductStockRequest request,
            CancellationToken cancellationToken)
        {
            await _mediator.Send(new ChangeProductStockCommand(request), cancellationToken);
            return Ok();
        }
    }
}
