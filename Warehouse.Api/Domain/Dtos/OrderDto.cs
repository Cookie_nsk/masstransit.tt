using System;

namespace Warehouse.Api.Domain.Entities
{
    public class OrderDto
    {
        public int Id { get; set; }

        public string ProductName { get; set; }

        public int Quantity { get; set; }

        public string State { get; set; }

        public int Number { get; set; }

        public string ReserveMode { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
