﻿using MongoDB.Bson.Serialization.Attributes;

namespace Warehouse.Api.Domain.Entities
{
    public class Category
    {
        [BsonId]
        public int Id { get; private set; }
        public int LowStockThreshold { get; private set; }
        public int OutOfStockThreshold { get; private set; }
        public string Name { get; private set; }

        public Category(int id, string name, int lowStockThreshold, int outOfStockThreshold)
        {
            Id = id;
            Name = name;
            LowStockThreshold = lowStockThreshold;
            OutOfStockThreshold = outOfStockThreshold;
        }
    }
}
