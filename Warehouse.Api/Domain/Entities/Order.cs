using MongoDB.Bson.Serialization.Attributes;
using System;
using Warehouse.Api.Domain.Enums;
using Warehouse.Api.Domain.Exceptions;

namespace Warehouse.Api.Domain.Entities
{
    public class Order
    {
        [BsonId]
        public int Id { get; private set; }
        public int ProductId { get; private set; }
        public int Quantity { get; private set; }
        public OrderStateEnum State { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public ReserveModeEnum ReserveMode { get; private set; }
        public DateTime UpdatedAt { get; private set; }

        public Order(int id, int productId, int quantity, OrderStateEnum state, ReserveModeEnum reserveMode)
        {
            Id = id;
            ProductId = productId;
            Quantity = quantity;
            State = state;
            CreatedAt = DateTime.UtcNow;
            ReserveMode = reserveMode;
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetStateApproved()
        {
            if (State != OrderStateEnum.ReviewInProgress)
                throw new InvalidOrderStateException(State, OrderStateEnum.Approved);

            State = OrderStateEnum.Approved;
            SetUpdatedAt(DateTime.UtcNow);
        }

        public void SetStateDeclined()
        {
            if (State != OrderStateEnum.ReviewInProgress)
                throw new InvalidOrderStateException(State, OrderStateEnum.Declined);

            State = OrderStateEnum.Declined;
            SetUpdatedAt(DateTime.UtcNow);
        }

        public void SetStateReviewInProgress()
        {
            State = OrderStateEnum.ReviewInProgress;
            SetUpdatedAt(DateTime.UtcNow);
        }

        public void SetUpdatedAt(DateTime utcDate)
        {
            UpdatedAt = utcDate;
        }
    }
}
