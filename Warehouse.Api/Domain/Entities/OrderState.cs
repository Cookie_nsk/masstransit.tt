﻿using MassTransit;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Warehouse.Api.Domain.Entities
{
    public class OrderState : SagaStateMachineInstance, ISagaVersion
    {
        [BsonId]
        public Guid CorrelationId { get; set; }
        public string SagaState { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int Version { get; set; }
        public DateTime CreatedAt { get; set; }

        public OrderState()
        {
            CreatedAt = DateTime.UtcNow;
        }
    }
}
