﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using Warehouse.Api.Domain.Enums;
using Warehouse.Api.Domain.Exceptions;

namespace Warehouse.Api.Domain.Entities
{
    public class Product
    {
        [BsonId]
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int Stock { get; private set; }
        public int CategoryId { get; private set; }
        [BsonIgnore]
        public Category Category { get; private set; }

        public DateTime UpdatedAt { get; private set; }

        public Product(int id, string name, int stock, int categoryId)
        {
            Id = id;
            Name = name;
            Stock = stock;
            CategoryId = categoryId;
            UpdatedAt = DateTime.UtcNow;
        }

        public ThresholdStateEnum CalculateLeftovers(int orderedQuantity)
        {
            int total = Stock - orderedQuantity;

            if (total > Category.LowStockThreshold)
            {
                return ThresholdStateEnum.Available;
            }
            else if (total >= Category.OutOfStockThreshold && total <= Category.LowStockThreshold)
            {
                return ThresholdStateEnum.LowStock;
            }

            return ThresholdStateEnum.OutOfStock;

        }

        public void Update(string name, int stock, int categoryId)
        {
            Name = name.Trim();
            Stock = stock;
            CategoryId = categoryId;
            SetUpdatedAt(DateTime.UtcNow);
        }

        public void SetCategory(Category category)
        {
            Category = category;
            SetUpdatedAt(DateTime.UtcNow);
        }

        public void ReserveItem(int quantity)
        {
            if (Stock - quantity < 0)
                throw new ProductOutOfStockException();

            Stock -= quantity;
            SetUpdatedAt(DateTime.UtcNow);
        }

        public void ReturnItemsToStock(int quantity)
        {
            Stock += quantity;
            SetUpdatedAt(DateTime.UtcNow);
        }

        public void SetStock(int quantity)
        {
            Stock = quantity;
            SetUpdatedAt(DateTime.UtcNow);
        }

        public void SetUpdatedAt(DateTime utcDate)
        {
            UpdatedAt = utcDate;
        }
    }
}
