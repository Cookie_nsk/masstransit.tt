﻿namespace Warehouse.Api.Domain.Enums
{
    public enum OrderStateEnum
    {
        OrderCreated,
        ReviewRequested,
        ReviewInProgress,
        Approved,
        Declined,
        WaitingForStock
    }
}
