﻿namespace Warehouse.Api.Domain.Enums
{
    public enum ThresholdStateEnum
    {
        Available,
        LowStock,
        OutOfStock,
    }
}
