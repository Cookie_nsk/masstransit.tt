﻿using System;
using Warehouse.Api.Domain.Enums;

namespace Warehouse.Api.Domain.Exceptions
{
    public class InvalidOrderStateException : Exception
    {
        public InvalidOrderStateException(OrderStateEnum currentState, OrderStateEnum changedState)
            : base($"Can not change the state to {changedState}. Current Order state {currentState} is invalid")
        { }
    }
}
