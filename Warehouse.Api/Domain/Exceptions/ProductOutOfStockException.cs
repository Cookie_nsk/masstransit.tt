﻿using System;

namespace Warehouse.Api.Domain.Exceptions
{
    public class ProductOutOfStockException : Exception
    {
        public ProductOutOfStockException() : base("Sorry, but product out of stock.")
        { }
    }
}
