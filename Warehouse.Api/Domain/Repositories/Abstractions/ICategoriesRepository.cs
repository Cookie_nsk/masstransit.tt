﻿using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Domain.Repositories.Abstractions
{
    public interface ICategoriesRepository
    {
        Task<Category> GetAsync(int id, CancellationToken cancellationToken);
        Task<Category> GetByNameAsync(string name, CancellationToken cancellationToken);
        Task AddAsync(Category category, CancellationToken cancellationToken);
        Task UpdateAsync(int id, string name, CancellationToken cancellationToken);
        Task DeleteAsync(int id, CancellationToken cancellationToken);
    }
}
