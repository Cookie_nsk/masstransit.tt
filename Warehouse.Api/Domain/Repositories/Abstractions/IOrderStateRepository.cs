﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Domain.Repositories.Abstractions
{
    public interface IOrderStateRepository
    {
        Task<OrderState> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<IEnumerable<OrderState>> GetWaitingForStockByParamsAsync(int productId, int quantity, CancellationToken cancellationToken);
    }
}
