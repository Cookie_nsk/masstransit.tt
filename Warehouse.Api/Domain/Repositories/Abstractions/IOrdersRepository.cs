﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Domain.Repositories.Abstractions
{
    public interface IOrdersRepository
    {
        Task<Order> GetAsync(int id, CancellationToken cancellationToken);
        Task<IEnumerable<OrderDto>> GetReportAsync(CancellationToken cancellationToken);
        Task<int> AddAsync(Order order, CancellationToken cancellationToken);
        Task UpdateAsync(Order order, CancellationToken cancellationToken);
    }
}
