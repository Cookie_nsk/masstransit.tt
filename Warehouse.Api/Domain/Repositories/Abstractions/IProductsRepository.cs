﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;

namespace Warehouse.Api.Domain.Repositories.Abstractions
{
    public interface IProductsRepository
    {
        Task<Product> GetWithCategoryAsync(int id, CancellationToken cancellationToken);
        Task<IEnumerable<Product>> GetAsync(IEnumerable<int> productIds, CancellationToken cancellationToken);
        Task<long> GetCountByNameAsync(string name, CancellationToken cancellationToken);
        Task AddAsync(Product product, CancellationToken cancellationToken);
        Task UpdateAsync(Product product, CancellationToken cancellationToken);
        Task<long> GetLinkedProductsCountByCategory(int categoryId, CancellationToken cancellationToken);
        Task UpdateStockAsync(Product product, CancellationToken cancellationToken);
    }
}
