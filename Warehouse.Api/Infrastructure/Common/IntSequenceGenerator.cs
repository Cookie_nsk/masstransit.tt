﻿using Forex.Infrastructure.MongoDb;
using MongoDB.Driver;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Infrastructure.Common;

namespace Forex.CaseApi.Infrastructure
{
    public interface IIntSequenceGenerator<T>
    {
        Task<int> NextAsync(CancellationToken cancellationToken);
    }

    public class IntSequenceGenerator<T> : IIntSequenceGenerator<T>
    {
        private readonly IMongoDbContext _mongoDbContext;

        public IntSequenceGenerator(IMongoDbContext mongoDbContext)
        {
            _mongoDbContext = mongoDbContext;
        }

        public async Task<int> NextAsync(CancellationToken cancellationToken)
        {
            var collection = _mongoDbContext.GetCollection<IntSequenceGeneratorState>();

            var updateDefinition = Builders<IntSequenceGeneratorState>.Update.Inc(x => x.SequenceNumber, 1);

            var filter = Builders<IntSequenceGeneratorState>.Filter.Where(x => x.Key == typeof(T).Name);

            var options = new FindOneAndUpdateOptions<IntSequenceGeneratorState, int>
            {
                IsUpsert = true,
                ReturnDocument = ReturnDocument.After,
                Projection = Builders<IntSequenceGeneratorState>.Projection.Expression(x => x.SequenceNumber)
            };

            return await collection.FindOneAndUpdateAsync(filter, updateDefinition, options, cancellationToken);
        }
    }
}
