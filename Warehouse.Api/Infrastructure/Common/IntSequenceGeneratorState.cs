﻿using MongoDB.Bson.Serialization.Attributes;

namespace Warehouse.Api.Infrastructure.Common
{
    public class IntSequenceGeneratorState
    {
        [BsonId]
        public string Key { get; set; }
        public int SequenceNumber { get; set; }
    }
}
