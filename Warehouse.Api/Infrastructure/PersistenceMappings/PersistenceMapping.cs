﻿using Forex.Infrastructure.MongoDb.Extensions;
using MongoDB.Bson.Serialization;
using System;

namespace Warehouse.Api.Infrastructure.PersistenceMappings
{
    public class PersistenceMapping
    {
        private readonly EntityMapper _entityMapper;
        public PersistenceMapping(EntityMapper entityMapper)
        {
            _entityMapper = entityMapper;
        }
        public void Register<T>(Action<BsonClassMap<T>> configuration)
        {
            var map = new BsonClassMap<T>();
            _entityMapper.Maps.Add(typeof(T), map);
            configuration(map);
        }
    }
}
