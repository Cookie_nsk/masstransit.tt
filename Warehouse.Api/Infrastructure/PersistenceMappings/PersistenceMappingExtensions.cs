﻿using Forex.Infrastructure.MongoDb.Extensions;
using Forex.Infrastructure.MongoDb.Internal;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Infrastructure.Common;

namespace Warehouse.Api.Infrastructure.PersistenceMappings
{
    public static class PersistenceMappingExtensions
    {
        public static PersistenceMapping RegisterMapping(this EntityMapper entityMapper)
        {
            return new PersistenceMapping(entityMapper)
                .AddMappings();
        }

        public static PersistenceMapping AddMappings(this PersistenceMapping persistenceMapping)
        {
            persistenceMapping.Register<Order>(cm =>
            {
                cm.SetCollectionName("orders");
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
                cm.MapIdProperty(x => x.Id);
            });

            persistenceMapping.Register<Category>(cm =>
            {
                cm.SetCollectionName("categories");
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
                cm.MapIdProperty(x => x.Id);
            });

            persistenceMapping.Register<Product>(cm =>
            {
                cm.SetCollectionName("products");
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
                cm.MapIdProperty(x => x.Id);
            });

            persistenceMapping.Register<OrderState>(cm =>
            {
                cm.SetCollectionName("orderState");
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
                cm.MapIdProperty(x => x.CorrelationId);
            });

            persistenceMapping.Register<IntSequenceGeneratorState>(cm =>
            {
                cm.SetCollectionName("IntSequenceGeneratorState");
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });

            return persistenceMapping;
        }
    }
}
