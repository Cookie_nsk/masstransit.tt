﻿using Forex.Infrastructure.MongoDb;
using MongoDB.Driver;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Infrastructure.Repositories
{
    public class CategoriesRepository : ICategoriesRepository
    {
        private readonly IMongoDbContext _mongoDbContext;
        private readonly IMongoCollection<Category> _collection;

        public CategoriesRepository(IMongoDbContext mongoDbContext)
        {
            _mongoDbContext = mongoDbContext;
            _collection = _mongoDbContext.GetCollection<Category>();
        }

        public async Task<Category> GetAsync(int id, CancellationToken cancellationToken)
        {
            return await (await _collection
                .FindAsync(x => x.Id == id, cancellationToken: cancellationToken))
                .FirstOrDefaultAsync(cancellationToken);
        }

        public async Task<Category> GetByNameAsync(string name, CancellationToken cancellationToken)
        {
            return (await _collection
                .FindAsync(x => x.Name == name.Trim(), cancellationToken: cancellationToken))
                .FirstOrDefault(cancellationToken);
        }

        public async Task AddAsync(Category category, CancellationToken cancellationToken)
        {
            await _collection.InsertOneAsync(category, cancellationToken: cancellationToken);
        }

        public async Task UpdateAsync(int id, string name, CancellationToken cancellationToken)
        {
            var update = Builders<Category>.Update.Set(x => x.Name, name);
            await _collection.UpdateOneAsync(x => x.Id == id, update);
        }

        public async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            await _collection.DeleteOneAsync(x => x.Id == id);
        }
    }
}
