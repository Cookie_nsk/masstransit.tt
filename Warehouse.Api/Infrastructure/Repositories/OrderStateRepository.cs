﻿using AutoMapper;
using Forex.Infrastructure.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Enums;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Infrastructure.Repositories
{
    public class OrderStateRepository : IOrderStateRepository
    {
        private readonly IMongoDbContext _mongoDbContext;
        private readonly IMongoCollection<OrderState> _collection;

        public OrderStateRepository(IMongoDbContext mongoDbContext, IMapper mapper)
        {
            _mongoDbContext = mongoDbContext;
            _collection = _mongoDbContext.GetCollection<OrderState>();
        }

        public async Task<OrderState> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            return (await _collection
                .FindAsync(x => x.CorrelationId == id, cancellationToken: cancellationToken))
                .FirstOrDefault();
        }

        public async Task<IEnumerable<OrderState>> GetWaitingForStockByParamsAsync(int productId, int quantity,
            CancellationToken cancellationToken)
        {
            return (await _collection
                .FindAsync(x => x.ProductId == productId &&
                x.SagaState == OrderStateEnum.WaitingForStock.ToString() &&
                x.Quantity <= quantity
                , cancellationToken: cancellationToken)).ToList();
        }
    }
}

