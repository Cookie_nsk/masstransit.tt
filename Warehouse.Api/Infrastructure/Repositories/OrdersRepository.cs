﻿using AutoMapper;
using Forex.Infrastructure.MongoDb;
using MassTransit;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Infrastructure.Repositories
{
    public class OrdersRepository : IOrdersRepository
    {
        private readonly IMongoDbContext _mongoDbContext;
        private readonly IMongoCollection<Order> _collection;
        private readonly IMapper _mapper;

        public OrdersRepository(IMongoDbContext mongoDbContext, IMapper mapper)
        {
            _mongoDbContext = mongoDbContext;
            _collection = _mongoDbContext.GetCollection<Order>();
            _mapper = mapper;
        }

        public async Task<Order> GetAsync(int id, CancellationToken cancellationToken)
        {
            return (await _collection
                .FindAsync(x => x.Id == id, cancellationToken: cancellationToken))
                .First(cancellationToken);
        }

        public async Task<IEnumerable<OrderDto>> GetReportAsync(CancellationToken cancellationToken)
        {
            var prodCollection = _mongoDbContext.GetCollection<Product>();

            var orders = _collection.AsQueryable().ToList();

            var productGuids = orders.Select(x => x.ProductId)
                .Distinct()
                .ToArray();

            var products = await prodCollection.Find(x => productGuids.Contains(x.Id)).ToListAsync();

            var retList = new List<OrderDto>();

            foreach (var order in orders)
            {
                var orderDto = new OrderDto();
                _mapper.Map(order, orderDto);
                _mapper.Map(products.FirstOrDefault(x => x.Id == order.ProductId), orderDto);
                retList.Add(orderDto);
            }

            return retList.ToArray();
        }

        public async Task<int> AddAsync(Order order, CancellationToken cancellationToken)
        {
            await _collection.InsertOneAsync(order, cancellationToken: cancellationToken);
            return order.Id;
        }

        public async Task UpdateAsync(Order order, CancellationToken cancellationToken)
        {
            var result = await _collection.ReplaceOneAsync(x => x.Id == order.Id, order);

            if (!result.IsAcknowledged)
                throw new InvalidOperationException("Update is not acknowledged");

            if (result.MatchedCount == 0 && result.UpsertedId == null)
                throw new ConcurrencyException();
        }
    }
}
