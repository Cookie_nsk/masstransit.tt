﻿using Forex.Infrastructure.MongoDb;
using MassTransit;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;

namespace Warehouse.Api.Infrastructure.Repositories
{
    public class ProductsRepository : IProductsRepository
    {
        private readonly IMongoDbContext _mongoDbContext;
        private readonly ICategoriesRepository _categRepository;
        private readonly IMongoCollection<Product> _collection;

        public ProductsRepository(IMongoDbContext mongoDbContext,
            ICategoriesRepository categRepository)
        {
            _mongoDbContext = mongoDbContext;
            _collection = _mongoDbContext.GetCollection<Product>();
            _categRepository = categRepository;
        }

        public async Task<Product> GetWithCategoryAsync(int id, CancellationToken cancellationToken)
        {
            var product = (await _collection
                .FindAsync(x => x.Id == id, cancellationToken: cancellationToken))
                .FirstOrDefault();
            product.SetCategory(await _categRepository.GetAsync(product.CategoryId, cancellationToken));

            return product;
        }

        public async Task<IEnumerable<Product>> GetAsync(IEnumerable<int> productIds, CancellationToken cancellationToken)
        {
            return await _collection
                .Find(x => productIds.Contains(x.Id))
                .ToListAsync(cancellationToken);
        }

        public async Task<long> GetCountByNameAsync(string name, CancellationToken cancellationToken)
        {
            return await _collection
                .CountDocumentsAsync(x => x.Name == name.Trim(), cancellationToken: cancellationToken);
        }

        public async Task AddAsync(Product product, CancellationToken cancellationToken)
        {
            await _collection.InsertOneAsync(product, cancellationToken: cancellationToken);
        }

        public async Task UpdateAsync(Product product, CancellationToken cancellationToken)
        {
            var filter = Builders<Product>.Filter.And(
                Builders<Product>.Filter.Eq(c => c.Id, product.Id),
                Builders<Product>.Filter.Lt(с => с.UpdatedAt, product.UpdatedAt));

            var result = await _collection.ReplaceOneAsync(filter, product);

            if (!result.IsAcknowledged)
                throw new InvalidOperationException("Update is not acknowledged");

            if (result.MatchedCount == 0 && result.UpsertedId == null)
                throw new ConcurrencyException();
        }

        public async Task<long> GetLinkedProductsCountByCategory(int categoryId, CancellationToken cancellationToken)
        {
            return await _collection
                .CountDocumentsAsync(x => x.CategoryId == categoryId, cancellationToken: cancellationToken);
        }

        public async Task UpdateStockAsync(Product product, CancellationToken cancellationToken)
        {
            var filter = Builders<Product>.Filter.And(
                Builders<Product>.Filter.Eq(c => c.Id, product.Id),
                Builders<Product>.Filter.Lt(с => с.UpdatedAt, product.UpdatedAt));

            var update = Builders<Product>.Update.Set(x => x.Stock, product.Stock);

            var result = await _collection.UpdateOneAsync(filter, update,
                new UpdateOptions { IsUpsert = false }, cancellationToken);

            if (!result.IsAcknowledged)
                throw new InvalidOperationException("Update is not acknowledged");

            if (result.MatchedCount == 0 && result.UpsertedId == null)
                throw new ConcurrencyException();
        }


    }
}
