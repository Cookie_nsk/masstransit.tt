﻿namespace Warehouse.Api.Options
{
    public class AppSettings
    {
        public MongoDb MongoDb { get; set; }
        public MassTransitSettings MassTransitSettings { get; set; }
        public RabbitMqSettings RabbitMqSettings { get; set; }
    }

    public class MongoDb
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public class MassTransitSettings
    {
        public string CollectionName { get; set; }
    }

    public class RabbitMqSettings
    {
        public string Host { get; set; }
        public string User { get; set; }
        public string Secret { get; set; }
        public string LocalEventsBusName { get; set; }
    }
}
