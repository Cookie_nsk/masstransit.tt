using FluentValidation.AspNetCore;
using Forex.CaseApi.Infrastructure;
using Forex.Infrastructure.MongoDb.Extensions;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using Warehouse.Api.Application.Consumers.EventHandlers;
using Warehouse.Api.Application.Sagas;
using Warehouse.Api.Application.Services.Abstractions;
using Warehouse.Api.Common.Middleware;
using Warehouse.Api.Domain.Entities;
using Warehouse.Api.Domain.Repositories.Abstractions;
using Warehouse.Api.Infrastructure.PersistenceMappings;
using Warehouse.Api.Infrastructure.Repositories;
using Warehouse.Api.Options;

namespace WarehouseApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WarehouseApi", Version = "v1" });
            });

            services.AddMongoDbClient(e => e.RegisterMapping());
            services.AddMediatR((opt) => { opt.AsTransient(); }, typeof(Startup));

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddFluentValidation(x => x.RegisterValidatorsFromAssemblyContaining<Startup>());

            RegisterServices(services);
            RegisterEventHandlers(services);
            RegisterMassTransit(services);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<GlobalExceptionMiddleware>();

            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WarehouseApi v1"));
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void RegisterServices(IServiceCollection services)
        {
            services.AddTransient<ICategoriesRepository, CategoriesRepository>();
            services.AddTransient<IOrdersRepository, OrdersRepository>();
            services.AddTransient<IOrderStateRepository, OrderStateRepository>();
            services.AddTransient<IProductsRepository, ProductsRepository>();
            services.AddTransient<IOrdersStatePublishingService, OrdersStatePublishingService>();
            services.AddScoped(typeof(IIntSequenceGenerator<>), typeof(IntSequenceGenerator<>));
        }

        private void RegisterEventHandlers(IServiceCollection services)
        {
            services.AddScoped<ApproveOrderEventHandler>();
            services.AddScoped<DeclineOrderEventHandler>();
            services.AddScoped<StockUpdatedEventHandler>();
            services.AddScoped<BeginUpdateOrderEventHandler>();
            services.AddScoped<BeginReviewAfterWaitingForStockEventHandler>();
        }

        private void RegisterMassTransit(IServiceCollection services)
        {
            var settings = Configuration.Get<AppSettings>();

            services.AddMassTransit(c =>
            {
                c.AddConsumer<BeginReviewAfterWaitingForStockEventHandler>();

                c.SetMongoDbSagaRepositoryProvider(configurator =>
                {
                    configurator.Connection = settings.MongoDb.ConnectionString;
                    configurator.DatabaseName = settings.MongoDb.DatabaseName;
                });

                c.AddSagaStateMachine<ReviewOrderWorkflow, OrderState>(c => c.UseInMemoryOutbox())
                    .MongoDbRepository(configurator =>
                    {
                        configurator.Connection = settings.MongoDb.ConnectionString;
                        configurator.DatabaseName = settings.MongoDb.DatabaseName;
                        configurator.CollectionName = settings.MassTransitSettings.CollectionName;
                    });

                c.UsingRabbitMq((context, configurator) =>
                {
                    configurator.Host(settings.RabbitMqSettings.Host, "/", h =>
                    {
                        h.Username(settings.RabbitMqSettings.User);
                        h.Password(settings.RabbitMqSettings.Secret);
                    });


                    string localFullName = $"rabbitmq://{settings.RabbitMqSettings.Host}/{settings.RabbitMqSettings.LocalEventsBusName}";

                    EndpointConvention.Map<ReviewOrderEvent>(new Uri(localFullName));

                    configurator.ReceiveEndpoint("reviewOrders", c => c.Consumer<ReviewOrderHandler>(context));

                    configurator.ReceiveEndpoint("common", c =>
                    {
                        c.Consumer<ReviewOrderHandler>(context);
                        c.Consumer<BeginReviewAfterWaitingForStockEventHandler>(context);
                        c.Consumer<ApproveOrderEventHandler>(context);
                        c.Consumer<DeclineOrderEventHandler>(context);
                    });

                    context.ConfigureEndpoints(configurator, null);
                });
            });
        }
    }
}
